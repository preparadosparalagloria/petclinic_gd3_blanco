package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class OwnerRepositoryTests {
	@Autowired
	private OwnerRepository owners;

	private Owner owner;

	@Before
	public void init() {
		if (this.owner == null) {
			owner = new Owner();
			owner.setFirstName("Vinzenzo");
			owner.setLastName("Nibali");
			owner.setTelephone("696696696");
			owner.setAddress("Calle Heraclio");
			owner.setCity("Coria");
			

			this.owners.save(owner);
		}
	}

	@Test
	public void deleteById() {
		int id = this.owner.getId();
		assertEquals(this.owners.findById(id),this.owner);
		this.owners.deleteById(id);
		assertNotEquals(this.owners.findById(id),this.owner);
		assertNull(this.owners.findById(id));
	}

}
