package org.springframework.samples.petclinic.vet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class VetRepositoryTest {
	
	@Autowired
	private VetRepository vetRepository;
	
	private Vet vet;
	
	@Before
	public void init() {
		if (this.vet==null) {
			vet = new Vet ();
			vet.setFirstName("Javier");
			vet.setLastName("Garcia");
			vet.setHomeVisits(false);
			vet.setId(3);

			this.vetRepository.save(vet);
		}
	}
	
	@Test
	public void testfindById() {
		Vet vetFindById = this.vetRepository.findById(vet.getId());
	
		assertEquals(vetFindById.getId(), vet.getId());
		
		if(vet.getId() == vetFindById.getId()) {
			assertEquals(vetFindById.getFirstName(), vet.getFirstName());
		}
		
		assertNotNull(vetFindById.getFirstName());
		assertEquals(vetFindById.getFirstName(), vet.getFirstName());	
		
		assertNotNull(vetFindById.getLastName());
		assertEquals(vetFindById.getLastName(), vet.getLastName());	
	}
	
	@Test
	public void findSpecialties() {
		Collection <Specialty> vetSpec = this.vetRepository.findSpecialties();
		assertEquals(vet.getNrOfSpecialties(),0);
		if (!vetSpec.isEmpty()) {				
			for (Specialty spec : vetSpec) {
				vet.addSpecialty(spec);
			}				
		}
		assertEquals(vet.getNrOfSpecialties(),vetSpec.size());
	}
	
	@Test
	public void findSpecialtiesById() {
		Collection <Specialty> vetSpec = this.vetRepository.findSpecialties();
		assertEquals(vet.getNrOfSpecialties(),0);
		if (!vetSpec.isEmpty()) {				
			for (Specialty spec : vetSpec) {
				assertTrue(spec == this.vetRepository.findSpecialtyById(spec.getId()));
				assertTrue(spec.getName() == this.vetRepository.findSpecialtyById(spec.getId()).getName());
				assertTrue(spec.getId() == this.vetRepository.findSpecialtyById(spec.getId()).getId());
			}				
		}
	}
}
