package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PetTest {
	public static Pet pet;
	public static Pet pet2;
	
	@BeforeClass
	//This method is called before executing this suite
	public static void initClass() {
		pet = new Pet();
	}
	
	@Before
	//This test is executed before each test created in this suite
	public void init() {
		pet.setWeight(4.3f);
		pet.setComments("No muerde");
	}
	

	@Test
	public void testVet() {
		assertNotNull(pet);				
	}
	
	@Test
	public void testVet2() {
		assertNull(pet2);				
	}
	
	@Test
	public void testGetWeight() {
		assertNotNull(pet.getWeight());
		assertNotEquals(pet.getWeight(), 3.1f);
		assertEquals(pet.getWeight(), 4.3f, 0.0);
	}

	@Test
	public void testSetWeight() {
		pet.setWeight(4.7f);
		assertNotNull(pet.getWeight());
		assertNotEquals(pet.getWeight(), 4.3f);
		assertEquals(pet.getWeight(), 4.7f, 0.0);
	}

	@Test
	public void testGetComments() {
		assertNotNull(pet.getComments());
		assertNotEquals(pet.getComments(), "Esta malo");
		assertEquals(pet.getComments(), "No muerde");
	}

	@Test
	public void testSetComments() {
		pet.setComments("Muerde");
		assertNotNull(pet.getComments());
		assertNotEquals(pet.getComments(), "No muerde");
		assertEquals(pet.getComments(), "Muerde");
	}

}

